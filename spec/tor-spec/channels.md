<a id="tor-spec.txt-2"></a>

# Channels {#channels}

A channel is a direct encrypted connection
between two Tor relays,
or between a client and a relay.

Channels are implemented as
[TLS](https://www.rfc-editor.org/rfc/rfc8446) sessions over TCP.

Clients and relays may both open new channels;
only a relay may be the recipient of a channel.

> Historical note:
> in some older documentation,
> channels were sometimes called "connections".
> This proved to be confusing,
> and we are trying not to use the term.

As part of establishing a channel,
the responding relay will always prove cryptographic ownership
of one or more [**relay identities**](./relay-keys.md),
using a [handshake](./negotiating-channels.md)
that combines TLS facilities
and a series of Tor messages.
The initiator MAY prove cryptographic ownership
of their own relay identities,
if they have any:
public relays SHOULD prove their identities when they initiate a channel,
whereas clients and bridges SHOULD NOT do so.

Parties should usually reuse an existing channel
rather than opening new a channel to the same relay.
There are exceptions here;
we discuss them more below. <!-- TODO: add anchor link -->

To open a channel,
a client or relay must know the IP address and port
of the target relay.
(This is sometimes called
the "OR address" or "OR port" for the relay.)
In most cases, the participant will also know
one or more expected identities for the target relay,
and will reject the channel
if the target relay cannot cryptographically prove
ownership of those identities.

> (When initiating a connection, if a reasonably live consensus is
> available, then the expected identity key is taken from that
> consensus. But when initiating a connection otherwise, the expected
> identity key is the one given in the hard-coded authority or
> fallback list.  Finally, when creating a connection because of an
> EXTEND/EXTEND2 cell, the expected identity key is the one given in
> the cell.)

<!-- TODO: Integrate that paragraph better, or put it in a better place. -->

Opening a channel is multi-step process:

1. The initiator opens
   a new TLS session with certain properties,
   and the responding relay checks and enforces those properties.
2. Both parties
   exchange messages over this TLS session
   in order to establish their identity or identities.
3. Both parties verify that the identities that they received
   are the ones that they expected.
   (If any expected key is missing or not as expected,
   the party MUST close the connection.)

Once this is done, the channel is Open,
and regular cells can be exchanged.

## Channel lifetime

Channels are not permanent.
Either side MAY close a channel if there are no circuits running on it
and an amount of time (KeepalivePeriod, defaults to 5 minutes)
has passed since the last time any traffic was transmitted over it.
Clients SHOULD
also hold a TLS connection with no circuits open, if it is likely that a
circuit will be built soon using that connection.

<!-- TODO: Give a forward-reference to explain predicted circuits
     and how we decide that a channel is "likely" to be used. -->

