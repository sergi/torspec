<a id="tor-spec.txt-4"></a>

# Negotiating and initializing channels {#negotiating}

Here we describe the primary TLS behavior
used by Tor relays and clients to create a new channel.
There are older versions of these handshakes,
which we describe in [another section](./obsolete-channels.md).

In brief:
 - The initiator starts the handshake
   by [opening a TLS connection](#tls).
 - Both parties send a [VERSIONS](#VERSIONS-cells)
   to negotiate the protocol version to use.
 - The responder sends a
   [CERTS cell](#CERTS-cells) to give the
   initiator the certificates it needs to learn the responder's
   identity, an [AUTH_CHALLENGE cell](#AUTH-CHALLENGE-cells)
   that the initiator must include
   as part of its answer if it chooses to authenticate, and a
   [NETINFO cell](#NETINFO-cells)
   to establish clock skew and IP addresses.
 - The initiator checks whether the CERTS cell is correct,
   and decides whether to authenticate.
 - If the initiator does not wants to authenticate,
   it sends a [NETINFO cell](#NETINFO-cells).
 - If the initiator wants to authenticate,
   it sends a [CERTS cell](#CERTS-cells),
   an [AUTHENTICATE cell](#AUTHENTICATE-cells),
   a [NETINFO cell](#NETINFO-cells).

When this handshake is in use,
the first cell must be VERSIONS, VPADDING, or AUTHORIZE,
and no other cell type is allowed to intervene besides those specified,
except for VPADDING cells.

(The AUTHORIZE cell type is reserved for future use
by scanning-resistance designs.
It is not specified here.)

```mermaid
sequenceDiagram
   Initiator --> Responder: TLS Handshake

   Note over Initiator,Responder: The rest is encrypted

   Initiator ->> Responder: VERSIONS
   Responder ->> Initiator: VERSIONS, CERTS, AUTH_CHALLENGE, NETINFO

   opt if the initiator wants to authenticate
      Initiator ->> Responder: CERTS, AUTHENTICATE
   end

   Initiator ->> Responder: NETINFO
```



## The TLS handshake {#tls}

<span id="in-protocol">The
initiator must send a ciphersuite list containing
at least one ciphersuite other than
[those listed in the obsolete v1 handshake](./obsolete-channels.md#v1-ciphersuite-list).
</span>

> This is trivially achieved
> by using any modern TLS implementation,
> and most implementations will not need to worry about it.
> 
> This requirement distinguishes the current protocol
> (sometimes called the "in-protocol" or "v3" handshake)
> from the obsolete v1 protocol.

<a id="tor-spec.txt-2.2"></a>

### TLS security considerations

(Standard TLS security guarantees apply;
this is not a comprehensive guide.)

Implementations SHOULD NOT allow TLS session resumption -- it can
exacerbate some attacks (e.g. the "Triple Handshake" attack from
Feb 2013), and it plays havoc with forward secrecy guarantees.

Implementations SHOULD NOT allow TLS compression -- although we don't
know a way to apply a CRIME-style attack to current Tor directly,
it's a waste of resources.

<a id="tor-spec.txt-4.1"></a>

## Negotiating versions with VERSIONS cells {#VERSIONS-cells}

There are multiple instances of the Tor channel protocol.

Once the TLS handshake is complete,
both parties send a VERSIONS cell
to negotiate which one they will use.

The payload in a VERSIONS cell is a series of big-endian two-byte
integers.
Both parties MUST select as the link protocol version the
highest number contained both in the VERSIONS cell they sent and in the
versions cell they received.
If they have no such version in common,
they cannot communicate and MUST close the connection.
Either party MUST
close the connection if the versions cell is not well-formed (for example,
if the payload contains an odd number of bytes).

Any VERSIONS cells sent after the first VERSIONS cell MUST be ignored.
(To be interpreted correctly, later VERSIONS cells MUST have a CIRCID_LEN
matching the version negotiated with the first VERSIONS cell.)

> (The
> [obsolete v1 channel protocol](./obsolete-channels.md#v1-handshake)
> does note VERSIONS cells.
> Implementations MUST NOT list version 1 in their VERSIONS cells.
> The
> [obsolete v2 channel protocol](./obsolete-channels.md#v2-handshake)
> can only be used after renegotiation;
> implementations MUST NOT list version 2 in their VERSIONS cells
> unless they have renegotiated the TLS session.)

The currently specified [Link](./subprotocol-versioning.md#link) protocols are:

| Version | Description
| ------- | -----------
| 1       | (Obsolete) The ["certs up front"](./obsolete-channels.md#v1-handshake) handshake.
| 2       | (Obsolete) Uses the [renegotiation-based handshake](./obsolete-channels.md#v2-handshake). Introduces variable-length cells.
| 3       | Uses the [current ("in-protocol") handshake](#in-protocol).  Must always be advertised.
| 4       | Increases circuit ID width to 4 bytes.
| 5       | Adds support for [link padding](../padding-spec) and negotiation.


<a id="tor-spec.txt-4.2"></a>

## CERTS cells {#CERTS-cells}

The CERTS cell describes the keys that a Tor instance is claiming
to have.  It is a variable-length cell.  Its payload format is:

| Field         | Description             | Size
| -----         | -----------             | ----
| N             | Number of certs in cell | 1 octet
| N times:      |                         |
| - CertType    |                         | 1 octet
| - CLEN        |                         | 2 octets
| - Certificate |                         | CLEN octets

Any extra octets at the end of a CERTS cell MUST be ignored.

Relevant certType values are:

| certType | Description
| -------- | -----------
| 1        | Link key certificate certified by RSA1024 identity
| 2        | RSA1024 Identity certificate, self-signed.
| 3        | RSA1024 AUTHENTICATE cell link certificate, signed with RSA1024 key.
| 4        | Ed25519 signing key, signed with identity key.
| 5        | TLS link certificate, signed with ed25519 signing key.
| 6        | Ed25519 AUTHENTICATE cell key, signed with ed25519 signing key.
| 7        | Ed25519 identity, signed with RSA identity.

The certificate format for certificate types 1-3 is DER encoded X509.
For others, the format is as documented in [a later section](../cert-spec.md)

Note that type 7 uses a different format from types 4-6.

A CERTS cell may have no more than one certificate of each CertType.

To authenticate the responder as having a given Ed25519,RSA identity key
combination, the initiator MUST check the following.

* The CERTS cell contains exactly one CertType 2 "ID" certificate.
* The CERTS cell contains exactly one CertType 4 Ed25519
 "Id->Signing" cert.
* The CERTS cell contains exactly one CertType 5 Ed25519
 "Signing->link" certificate.
* The CERTS cell contains exactly one CertType 7 "RSA->Ed25519"
 cross-certificate.
* All X.509 certificates above have validAfter and validUntil dates;
 no X.509 or Ed25519 certificates are expired.
* All certificates are correctly signed.
* The certified key in the Signing->Link certificate matches the
 SHA256 digest of the certificate that was used to
 authenticate the TLS connection.
* The identity key listed in the ID->Signing cert was used to
 sign the ID->Signing Cert.
* The Signing->Link cert was signed with the Signing key listed
 in the ID->Signing cert.
* The RSA->Ed25519 cross-certificate certifies the Ed25519
 identity, and is signed with the RSA identity listed in the
 "ID" certificate.
* The certified key in the ID certificate is a 1024-bit RSA key.
* The RSA ID certificate is correctly self-signed.

To authenticate the responder as having a given RSA identity only,
the initiator MUST check the following:

* The CERTS cell contains exactly one CertType 1 "Link" certificate.
* The CERTS cell contains exactly one CertType 2 "ID" certificate.
* Both certificates have validAfter and validUntil dates that
 are not expired.
* The certified key in the Link certificate matches the
 link key that was used to negotiate the TLS connection.
* The certified key in the ID certificate is a 1024-bit RSA key.
* The certified key in the ID certificate was used to sign both
 certificates.
* The link certificate is correctly signed with the key in the
 ID certificate
* The ID certificate is correctly self-signed.

In both cases above, checking these conditions is sufficient to
authenticate that the initiator is talking to the Tor node with the
expected identity, as certified in the ID certificate(s).

To authenticate the initiator as having a given Ed25519,RSA
identity key combination, the responder MUST check the following:

* The CERTS cell contains exactly one CertType 2 "ID" certificate.
* The CERTS cell contains exactly one CertType 4 Ed25519
 "Id->Signing" certificate.
* The CERTS cell contains exactly one CertType 6 Ed25519
 "Signing->auth" certificate.
* The CERTS cell contains exactly one CertType 7 "RSA->Ed25519"
 cross-certificate.
* All X.509 certificates above have validAfter and validUntil dates;
 no X.509 or Ed25519 certificates are expired.
* All certificates are correctly signed.
* The identity key listed in the ID->Signing cert was used to
 sign the ID->Signing Cert.
* The Signing->AUTH cert was signed with the Signing key listed
 in the ID->Signing cert.
* The RSA->Ed25519 cross-certificate certifies the Ed25519
 identity, and is signed with the RSA identity listed in the
 "ID" certificate.
* The certified key in the ID certificate is a 1024-bit RSA key.
* The RSA ID certificate is correctly self-signed.

To authenticate the initiator as having an RSA identity key only,
the responder MUST check the following:

* The CERTS cell contains exactly one CertType 3 "AUTH" certificate.
* The CERTS cell contains exactly one CertType 2 "ID" certificate.
* Both certificates have validAfter and validUntil dates that
 are not expired.
* The certified key in the AUTH certificate is a 1024-bit RSA key.
* The certified key in the ID certificate is a 1024-bit RSA key.
* The certified key in the ID certificate was used to sign both
 certificates.
* The auth certificate is correctly signed with the key in the
 ID certificate.
* The ID certificate is correctly self-signed.

Checking these conditions is NOT sufficient to authenticate that the
initiator has the ID it claims; to do so, [AUTH_CHALLENGE](#AUTH-CHALLENGE-cells)
and [AUTHENTICATE](#AUTHENTICATE-cells) cells (described next) must be exchanged.

<a id="tor-spec.txt-4.3"></a>

## AUTH_CHALLENGE cells{#AUTH-CHALLENGE-cells}

An AUTH_CHALLENGE cell is a variable-length cell with the following
fields:

| Field     | Size
| -----     | ----
| Challenge | 32 octets
| N_Methods | 2 octets
| Methods   | 2 * N_Methods octets

It is sent from the responder to the initiator. Initiators MUST
ignore unexpected bytes at the end of the cell.  Responders MUST
generate every challenge independently using a strong RNG or PRNG.

The Challenge field is a randomly generated string that the
initiator must sign (a hash of) as part of authenticating.  The
methods are the authentication methods that the responder will
accept.  Only two authentication methods are defined right now:
see [RSA-SHA256-TLSSecret](#RSA-SHA256-TLSSecret) and
[Ed25519-SHA256-RFC570](#Ed25519-SHA256-RFC5705) below.

<a id="tor-spec.txt-4.4"></a>

## AUTHENTICATE cells{#AUTHENTICATE-cells}

If an initiator wants to authenticate, it responds to the
AUTH_CHALLENGE cell with a CERTS cell and an AUTHENTICATE cell.
The CERTS cell is as a server would send, except that instead of
sending a CertType 1 (and possibly CertType 5) certs for arbitrary link
certificates, the initiator sends a CertType 3 (and possibly
CertType 6) cert for an RSA/Ed25519 AUTHENTICATE key.

This difference is because we allow any link key type on a TLS
link, but the protocol described here will only work for specific key
types as described in
[RSA-SHA256-TLSSecret](#RSA-SHA256-TLSSecret) and
[Ed25519-SHA256-RFC570](#Ed25519-SHA256-RFC5705) below.

An AUTHENTICATE cell contains the following:

| Field           | Size
| -----           | ----
| AuthType        | 2 octets
| AuthLen         | 2 octets
| Authentication  | AuthLen octets

Responders MUST ignore extra bytes at the end of an AUTHENTICATE
cell.  Recognized AuthTypes are 1 and 3, described in the next
two sections.

Initiators MUST NOT send an AUTHENTICATE cell before they have
verified the certificates presented in the responder's CERTS
cell, and authenticated the responder.

<a id="tor-spec.txt-4.4.1"></a>

### Link authentication type 1: RSA-SHA256-TLSSecret {#RSA-SHA256-TLSSecret}

If AuthType is 1 (meaning "RSA-SHA256-TLSSecret"), then the
Authentication field of the AUTHENTICATE cell contains the following:

* TYPE: The characters "AUTH0001" \[8 octets\]
* CID: A SHA256 hash of the initiator's RSA1024 identity key \[32 octets\]
* SID: A SHA256 hash of the responder's RSA1024 identity key \[32 octets\]
* SLOG: A SHA256 hash of all bytes sent from the responder to the
    initiator as part of the negotiation up to and including the
    AUTH_CHALLENGE cell; that is, the VERSIONS cell, the CERTS cell,
    the AUTH_CHALLENGE cell, and any padding cells.  \[32 octets\]
* CLOG: A SHA256 hash of all bytes sent from the initiator to the
    responder as part of the negotiation so far; that is, the
    VERSIONS cell and the CERTS cell and any padding cells. \[32
    octets\]
* SCERT: A SHA256 hash of the responder's TLS link certificate. \[32
    octets\]
* TLSSECRETS: A SHA256 HMAC, using the TLS master secret as the
    secret key, of the following:
      - client_random, as sent in the TLS Client Hello
      - server_random, as sent in the TLS Server Hello
      - the NUL terminated ASCII string:
        "Tor V3 handshake TLS cross-certification"
     \[32 octets\]
* RAND: A 24 byte value, randomly chosen by the initiator.  (In an
    imitation of SSL3's gmt_unix_time field, older versions of Tor
    sent an 8-byte timestamp as the first 8 bytes of this field;
    new implementations should not do that.) \[24 octets\]
* SIG: A signature of a SHA256 hash of all the previous fields
    using the initiator's "Authenticate" key as presented.  (As
    always in Tor, we use OAEP-MGF1 padding; see [Ciphers](./preliminaries.md#ciphers))
     \[variable length\]

To check the AUTHENTICATE cell, a responder checks that all fields
from TYPE through TLSSECRETS contain their unique
correct values as described above, and then verifies the signature.
The server MUST ignore any extra bytes in the signed data after
the RAND field.

Responders MUST NOT accept this AuthType if the initiator has
claimed to have an Ed25519 identity.

(There is no AuthType 2: It was reserved but never implemented.)

<a id="tor-spec.txt-4.4.2"></a>

### Link authentication type 3: Ed25519-SHA256-RFC5705 {#Ed25519-SHA256-RFC5705}

If AuthType is 3, meaning "Ed25519-SHA256-RFC5705", the
Authentication field of the AuthType cell is as below:

Modified values and new fields below are marked with asterisks.

* TYPE: The characters "AUTH0003" \[8 octets\]
* CID: A SHA256 hash of the initiator's RSA1024 identity key \[32 octets\]
* SID: A SHA256 hash of the responder's RSA1024 identity key \[32 octets\]
* CID_ED: The initiator's Ed25519 identity key \[32 octets\]
* SID_ED: The responder's Ed25519 identity key, or all-zero. \[32 octets\]
* SLOG: A SHA256 hash of all bytes sent from the responder to the
    initiator as part of the negotiation up to and including the
    AUTH_CHALLENGE cell; that is, the VERSIONS cell, the CERTS cell,
    the AUTH_CHALLENGE cell, and any padding cells.  \[32 octets\]
* CLOG: A SHA256 hash of all bytes sent from the initiator to the
    responder as part of the negotiation so far; that is, the
    VERSIONS cell and the CERTS cell and any padding cells. \[32
    octets\]
* SCERT: A SHA256 hash of the responder's TLS link certificate. \[32
    octets\]
* TLSSECRETS: The output of an RFC5705 Exporter function on the
    TLS session, using as its inputs:
     - The label string "EXPORTER FOR TOR TLS CLIENT BINDING AUTH0003"
     - The context value equal to the initiator's Ed25519 identity key.
     - The length 32.
       \[32 octets\]
* RAND: A 24 byte value, randomly chosen by the initiator. \[24 octets\]
* SIG: A signature of all previous fields using the initiator's
     Ed25519 authentication key (as in the cert with CertType 6).
     \[variable length\]

To check the AUTHENTICATE cell, a responder checks that all fields
from TYPE through TLSSECRETS contain their unique
correct values as described above, and then verifies the signature.
The server MUST ignore any extra bytes in the signed data after
the RAND field.

<a id="tor-spec.txt-4.5"></a>

## NETINFO cells {#NETINFO-cells}

If version 2 or higher is negotiated, each party sends the other a
NETINFO cell.  The cell's payload is:

| Field            | Description                   | Size
| -----            | -----------                   | ----
| TIME             | Timestamp                     | 4 bytes
| OTHERADDR:       | Other OR's address            |
| - ATYPE          | Address type                  | 1 byte
| - ALEN           | Address length                | 1 byte
| - AVAL           | Address value in NBO          | ALEN bytes
| NMYADDR          | Number of this OR's addresses | 1 byte
| NMYADDR times:   |                               |
| - ATYPE          | Address type                  | 1 byte
| - ALEN           | Address length                | 1 byte
| - AVAL           | Address value in NBO          | ALEN bytes

Recognized address types (ATYPE) are:

| ATYPE | Description
| ----- | -----------
| 0x04  | IPv4
| 0x06  | IPv6

ALEN MUST be 4 when ATYPE is 0x04 (IPv4) and 16 when ATYPE is 0x06
(IPv6).  If the ALEN value is wrong for the given ATYPE value, then
the provided address should be ignored.

The timestamp is a big-endian unsigned integer number of seconds
since the Unix epoch. Implementations MUST ignore unexpected bytes
at the end of the cell.  Clients SHOULD send "0" as their timestamp, to
avoid fingerprinting.

Implementations MAY use the timestamp value to help decide if their
clocks are skewed.  Initiators MAY use "other OR's address" to help
learn which address their connections may be originating from, if they do
not know it; and to learn whether the peer will treat the current
connection as canonical.  Implementations SHOULD NOT trust these
values unconditionally, especially when they come from non-authorities,
since the other party can lie about the time or IP addresses it sees.

Initiators SHOULD use "this OR's address" to make sure
that they have connected to another OR at its canonical address.
(See [Canonical connections](./creating-circuits.md#canonical-connections))
