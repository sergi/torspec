# Obsolete channel handshakes

These handshake variants are no longer in use.
Channel initiators MUST NOT send them.
Relays MAY detect and reject them.

> If you are experienced with TLS,
> you will find some aspects of this handshake
> strange or obfuscated.
> Several historical factors led to its current state.
>
> First, before the development of
> [pluggable transports](../pt-spec/),
> Tor tried to avoid censorship by mimicking the behavior
> of a web client negotiating HTTPS with a web server.
> If we wanted a secure option that was not in common use,
> we had to hide our use of that option.
>
> Second, prior to the introduction of
> [TLS 1.3](https://datatracker.ietf.org/doc/html/rfc8446),
> many more aspects of the handshake
> (such as the number and nature of certificates sent by each party)
> were sent in the clear,
> and were easy to distinguish.
>
> Third, prior to the introduction of TLS 1.3,
> there was no good encrypted signalling mechanism
> that a client could use to declare
> how it wanted the rest of the TLS handshake to proceed.
> Thus, we wound up using the client's list
> of supported ciphersuites
> to send a signal about which variation of the handshake is in use.


### Version 1, or "certificates up front" {#v1-handshake}

With this obsolete handshake,
the responding relay proves ownership of an RSA identity (`KP_relayid_rsa`),
and the initiator also proves ownership of an RSA identity.

(If the initiator does not have an RSA identity to prove,
it invents one and throws it away afterwards.)

To select this handshake,
the initiator starts a TLS handshake
containing no ciphersuites other than these:

<a id="v1-ciphersuite-list"></a>
```text
     TLS_DHE_RSA_WITH_AES_256_CBC_SHA
     TLS_DHE_RSA_WITH_AES_128_CBC_SHA
     SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA
```

> Note that because of this list,
> it is impossible to use this obsolete handshake
> with TLS 1.3.


As part of the TLS handshake,
the initiator sends a two-certificate chain,
consisting of an X.509 certificate
for its short-term connection public key (`KP_legacy_conn_tls`)
signed by `KP_relayid_rsa`, and
a second self-signed X.509 certificate containing `KP_relayid_rsa`.
The responder sends a similar
certificate chain.

Once the TLS handshake is done,
both parties validate each other's certificate chains.
If they are valid,
then the connection is Open,
and both parties may start exchanging [cells](./cell-packet-format.md).

## Version 2, or "renegotiation" {#v2-handshake}

In "renegotiation" (a.k.a. "the v2 handshake"),
the connection initiator selects at least one ciphersuite
not in the [list above](#v1-ciphersuite-list).
The initiator sends no certificates, and
the responder sends a single connection certificate in return.

(If the responder sends a certificate chain,
the initiator assumes that it only knows about the v1 handshake.)

Once this initial TLS handshake is complete,
the initiator renegotiates the session.
During the renegotiation,
each party sends a two-certificate chain
as in the ["certificates up front"](#v1-handshake) handshake above.

When this handshake is used,
both parties immediately
send a VERSIONS cell, and after negotiating a link
protocol version (which will be 2), each sends a NETINFO cell
to confirm their addresses and timestamps.
At that point, the channel is Open.
No other intervening cell types are allowed.

## Indicating support for the in-protocol handshake

When the in-protocol handshake was new,
we placed a set of constraints on the certificate that the responder would send
to indicate that it supported the v3 handshake.

Specifically, if at least one of these properties
was true of the responders's certificate,
the initiator could be sure that the responder supported
the in-protocol handshake:

- The certificate is self-signed
- Some component other than "commonName" is set in the subject or
  issuer DN of the certificate.
- The commonName of the subject or issuer of the certificate ends
  with a suffix other than ".net".
- The certificate's public key modulus is longer than 1024 bits.

Otherwise, the initiator would assume
that only the v2 protocol was in use.

## Fixed ciphersuite list {#fixed-cipehrsuite-list}

For a long time, clients would advertise
a certain "fixed ciphersuite list"
regardless of whether they actually supported those ciphers.

That list is:

```text
     TLS1_ECDHE_ECDSA_WITH_AES_256_CBC_SHA
     TLS1_ECDHE_RSA_WITH_AES_256_CBC_SHA
     TLS1_DHE_RSA_WITH_AES_256_SHA
     TLS1_DHE_DSS_WITH_AES_256_SHA
     TLS1_ECDH_RSA_WITH_AES_256_CBC_SHA
     TLS1_ECDH_ECDSA_WITH_AES_256_CBC_SHA
     TLS1_RSA_WITH_AES_256_SHA
     TLS1_ECDHE_ECDSA_WITH_RC4_128_SHA
     TLS1_ECDHE_ECDSA_WITH_AES_128_CBC_SHA
     TLS1_ECDHE_RSA_WITH_RC4_128_SHA
     TLS1_ECDHE_RSA_WITH_AES_128_CBC_SHA
     TLS1_DHE_RSA_WITH_AES_128_SHA
     TLS1_DHE_DSS_WITH_AES_128_SHA
     TLS1_ECDH_RSA_WITH_RC4_128_SHA
     TLS1_ECDH_RSA_WITH_AES_128_CBC_SHA
     TLS1_ECDH_ECDSA_WITH_RC4_128_SHA
     TLS1_ECDH_ECDSA_WITH_AES_128_CBC_SHA
     SSL3_RSA_RC4_128_MD5
     SSL3_RSA_RC4_128_SHA
     TLS1_RSA_WITH_AES_128_SHA
     TLS1_ECDHE_ECDSA_WITH_DES_192_CBC3_SHA
     TLS1_ECDHE_RSA_WITH_DES_192_CBC3_SHA
     SSL3_EDH_RSA_DES_192_CBC3_SHA
     SSL3_EDH_DSS_DES_192_CBC3_SHA
     TLS1_ECDH_RSA_WITH_DES_192_CBC3_SHA
     TLS1_ECDH_ECDSA_WITH_DES_192_CBC3_SHA
     SSL3_RSA_FIPS_WITH_3DES_EDE_CBC_SHA
     SSL3_RSA_DES_192_CBC3_SHA
     [*] The "extended renegotiation is supported" ciphersuite, 0x00ff, is
         not counted when checking the list of ciphersuites.
```

When encountering this list,
a responder would not select any ciphersuites
besides the mandatory-to-implement
TLS_DHE_RSA_WITH_AES_256_CBC_SHA, TLS_DHE_RSA_WITH_AES_128_CBC_SHA,
and SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA.

Clients no longer report ciphers that they do not support.
